#ifndef CACHESIM_H
#define CACHESIM_H

#define MEMADDR 64


#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>


typedef struct _cache_line {
    uint64_t tag;
    //void data;
    int valid;
    int dirty;
    int prefetched;
} cache_line;

typedef struct _lru_block {
    uint64_t lru_tag;
    struct lru_block *next;
} lru_block;

typedef struct _set {
    cache_line *lines;
    lru_block *lru;
} set;

typedef struct _vc_queue {
    cache_line line;
    uint64_t idx;
    struct vc_queue *next;
} vc_queue;

typedef struct _vc {
    vc_queue *q;
    uint64_t v;
} vc;

typedef struct _prefetcher {
    int pending_stride;
    uint64_t last_miss_baddr;
    uint64_t k;
} prefetcher;

typedef struct _cache {
    uint64_t c;
    uint64_t s;
    uint64_t b;
    char type;
    uint64_t idx_bits;

    /*  d for for direct mapped
        f for fully associative
        a for any other type of associativity
    */
    set *sets;
} cache;

cache *cache_instance;
vc *vc_instance;
prefetcher *pf_instance;

struct cache_stats_t {
    uint64_t accesses;
    uint64_t reads;
    uint64_t read_misses;
    uint64_t read_misses_combined;
    uint64_t writes;
    uint64_t write_misses;
    uint64_t write_misses_combined;
    uint64_t misses;
	uint64_t write_backs;
	uint64_t vc_misses;
	uint64_t prefetched_blocks;
	uint64_t useful_prefetches;
	uint64_t bytes_transferred; 
    uint64_t evictions;
	float hit_time;
    uint64_t miss_penalty;
    double   miss_rate;
    double   avg_access_time;
};

void cache_access(char rw, uint64_t address, struct cache_stats_t* p_stats);
void setup_cache(uint64_t c, uint64_t b, uint64_t s, uint64_t v, uint64_t k);
void complete_cache(struct cache_stats_t *p_stats);

static const uint64_t DEFAULT_C = 15;   /* 32KB Cache */
static const uint64_t DEFAULT_B = 5;    /* 32-byte blocks */
static const uint64_t DEFAULT_S = 3;    /* 8 blocks per set */
static const uint64_t DEFAULT_V = 4;    /* 4 victim blocks */
static const uint64_t DEFAULT_K = 2;	/* 2 prefetch distance */

/** Argument to cache_access rw. Indicates a load */
static const char     READ = 'r';
/** Argument to cache_access rw. Indicates a store */
static const char     WRITE = 'w';

#endif /* CACHESIM_H */
