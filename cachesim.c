#include "cachesim.h"


/**
 * Subroutine for initializing the cache. You many add and initialize any global or heap
 * variables as needed.
 * XXX: You're responsible for completing this routine
 *
 * @c The total number of bytes for data storage is 2^C
 * @b The size of a single cache line in bytes is 2^B
 * @s The number of blocks in each set is 2^S
 * @v The number of blocks in the victim cache is 2^V
 * @k The prefetch distance is K
 */
void init_vcache(uint64_t v)
{
	vc_instance = malloc(sizeof(vc));
	vc_instance->v = v;

}
void init_prefetcher(uint64_t k)
{
	pf_instance = malloc(sizeof(prefetcher));
	pf_instance->k=k;
	pf_instance->last_miss_baddr=0;
	pf_instance->pending_stride=0;

}


void setup_cache(uint64_t c, uint64_t b, uint64_t s, uint64_t v, uint64_t k) {
	cache_instance = malloc(sizeof(cache));
	if(c > 0 && b > 0 && s >= 0)
	{
		uint64_t i,j;
		uint64_t num_lines = pow(2,(c-b));
		uint64_t num_sets = pow(2,(c-(b+s)));

		cache_instance->c=c;
		cache_instance->s=s;
		cache_instance->b=b;

		if(s==0) {
			cache_instance->type='d';
			cache_instance->idx_bits = cache_instance->c - cache_instance->b;
			cache_instance->sets = malloc(sizeof(set));
			cache_instance->sets->lines = malloc(sizeof(cache_line)*num_lines);
			for(i=0;i<num_lines;++i) {
				cache_instance->sets->lines[i].valid=0;
				cache_instance->sets->lines[i].dirty=0;
				cache_instance->sets->lines[i].prefetched=0;
			}



		}
		else if(s==(c-b)) {
			cache_instance->type='f';
			cache_instance->idx_bits = 0;
			cache_instance->sets = malloc(sizeof(set));
			for(i=0; i<num_sets; ++i)
				cache_instance->sets->lines=malloc(sizeof(cache_line)*pow(2,s));	
			for(i=0;i<pow(2,s);++i) {
				cache_instance->sets->lines[i].valid=0;
				cache_instance->sets->lines[i].dirty=0;
				cache_instance->sets->lines[i].prefetched=0;
			}
		}
		else {
			cache_instance->type='a';
			cache_instance->idx_bits = cache_instance->c - (cache_instance->b + cache_instance->s);

			cache_instance->sets = malloc(sizeof(set)*(num_sets));
			for(i=0; i<num_sets; ++i) {
				cache_instance->sets[i].lines=malloc(sizeof(cache_line)*pow(2,s));			
					for(j=0;j<pow(2,s);++j) {
						cache_instance->sets[i].lines[j].valid=0;
						cache_instance->sets[i].lines[j].dirty=0;
						cache_instance->sets[i].lines[j].prefetched=0;
					}
			}


		}
		
		
		/*V and k stuff goes here: */
		init_vcache(v);
		init_prefetcher(k);



	}
	else
		fprintf(stderr,"ERROR: Invalid cache/ block or set size\n");


}



void insert_lru(uint64_t tag, uint64_t idx)
{
	lru_block *newnode=malloc(sizeof(lru_block*));
	newnode->lru_tag=tag;
	newnode->next=NULL;
	lru_block *insert_at;
	if(cache_instance->type == 'f') {
		insert_at=cache_instance->sets->lru;
		if(insert_at==NULL){
			cache_instance->sets->lru=newnode;
		}
		else {
			while(insert_at->next!=NULL)
				insert_at=insert_at->next;
			insert_at->next=newnode;
		}

	}
	else {
		insert_at=cache_instance->sets[idx].lru;
		if(insert_at==NULL){
			cache_instance->sets[idx].lru=newnode;

		}
		else {
			while(insert_at->next!=NULL) {
				insert_at=insert_at->next;

			}
			insert_at->next=newnode;
		}
	
	}
		
}


int check_vc(uint64_t tag, uint64_t idx)
{
	int ret=0;

	if(vc_instance->v > 0)
	{

		vc_queue *ptr = vc_instance->q;

		if(ptr!=NULL)
		{
			while(ptr->next!=NULL) {
				if(ptr->line.tag==tag && ptr->line.valid==1 && ptr->idx==idx)
					ret=1;
				ptr=ptr->next;
			}
		}
	}
	return ret;
}


void swap_blocks_vc(uint64_t tag, uint64_t idx, int dest_j)
{
	vc_queue *ptr = vc_instance->q;
	cache_line temp_line;
	while(ptr->next!=NULL && ptr->line.tag!=tag && ptr->line.valid!=1 && ptr->idx!=idx) {
				if(ptr->line.tag==tag && ptr->line.valid==1 && ptr->idx==idx)
					break;
				else
					ptr=ptr->next;
	
				
	}
  
	if(cache_instance->type=='d')
	{
		
		if(cache_instance->sets->lines[idx].valid!=1)
		{
			cache_instance->sets->lines[idx].tag=tag;
			cache_instance->sets->lines[idx].valid=1;
			cache_instance->sets->lines[idx].dirty=ptr->line.dirty;
			cache_instance->sets->lines[idx].prefetched=ptr->line.prefetched;
		}
		else {
			
			temp_line.tag=cache_instance->sets->lines[idx].tag;
			temp_line.dirty=cache_instance->sets->lines[idx].dirty;
			temp_line.valid=cache_instance->sets->lines[idx].valid;
			temp_line.prefetched=cache_instance->sets->lines[idx].prefetched;
			cache_instance->sets->lines[idx].tag=tag;
			cache_instance->sets->lines[idx].valid=1;
			cache_instance->sets->lines[idx].dirty=ptr->line.dirty;
			cache_instance->sets->lines[idx].prefetched=ptr->line.prefetched;
			ptr->line.tag=temp_line.tag;
			ptr->line.valid=1;
			ptr->line.dirty=temp_line.dirty;
			ptr->line.prefetched=temp_line.prefetched;
			ptr->idx=idx;
		}
				
	}
	else if(cache_instance->type=='f')
	{
		if(cache_instance->sets->lines[dest_j].valid!=1)
		{
			cache_instance->sets->lines[dest_j].tag=tag;
			cache_instance->sets->lines[dest_j].valid=1;
			cache_instance->sets->lines[dest_j].dirty=ptr->line.dirty;	
			cache_instance->sets->lines[dest_j].prefetched=ptr->line.prefetched;
		}
		else
		{
			temp_line.tag=cache_instance->sets->lines[dest_j].tag;
			temp_line.dirty=cache_instance->sets->lines[dest_j].dirty;
			temp_line.valid=cache_instance->sets->lines[dest_j].valid;
			temp_line.prefetched=cache_instance->sets->lines[dest_j].prefetched;
			cache_instance->sets->lines[dest_j].tag=tag;
			cache_instance->sets->lines[dest_j].valid=1;
			cache_instance->sets->lines[dest_j].dirty=ptr->line.dirty;
			cache_instance->sets->lines[dest_j].prefetched=ptr->line.prefetched;
			ptr->line.tag=temp_line.tag;
			ptr->line.valid=1;
			ptr->line.dirty=temp_line.dirty;
			ptr->line.prefetched=temp_line.prefetched;
			ptr->idx=idx;

		}
	}
	else
	{
		if(cache_instance->sets[idx].lines[dest_j].valid!=1)
		{
			cache_instance->sets[idx].lines[dest_j].tag=tag;
			cache_instance->sets[idx].lines[dest_j].valid=1;
			cache_instance->sets[idx].lines[dest_j].dirty=ptr->line.dirty;	
			cache_instance->sets[idx].lines[dest_j].prefetched=ptr->line.prefetched;
		}
		else
		{
			temp_line.tag=cache_instance->sets[idx].lines[dest_j].tag;
			temp_line.dirty=cache_instance->sets[idx].lines[dest_j].dirty;
			temp_line.valid=cache_instance->sets[idx].lines[dest_j].valid;
			temp_line.prefetched=cache_instance->sets[idx].lines[dest_j].prefetched;
			cache_instance->sets[idx].lines[dest_j].tag=tag;
			cache_instance->sets[idx].lines[dest_j].valid=1;
			cache_instance->sets[idx].lines[dest_j].dirty=ptr->line.dirty;
			cache_instance->sets[idx].lines[dest_j].prefetched=ptr->line.prefetched;
			ptr->line.tag=temp_line.tag;
			ptr->line.valid=1;
			ptr->line.dirty=temp_line.dirty;
			ptr->line.prefetched=temp_line.prefetched;
			ptr->idx=idx;
		}	
	}
  


}

int update_vc(uint64_t tag, uint64_t idx, int dest_j,struct cache_stats_t* p_stats)
{
	int ret=1,i=0;

	if(vc_instance->v > 0)
	{
		vc_queue *ptr = vc_instance->q;
		if(ptr==NULL)
		{
			vc_instance->q = malloc(sizeof(vc_queue));
			vc_instance->q->line.tag=tag;
			if(cache_instance->type=='d')
			{
				vc_instance->q->line.dirty=cache_instance->sets->lines[idx].dirty;
				vc_instance->q->line.valid=cache_instance->sets->lines[idx].valid;
				vc_instance->q->line.prefetched=cache_instance->sets->lines[idx].prefetched;
			}
			else if(cache_instance->type=='f')
			{
				vc_instance->q->line.dirty=cache_instance->sets->lines[dest_j].dirty;
				vc_instance->q->line.valid=cache_instance->sets->lines[dest_j].valid;	
				vc_instance->q->line.prefetched=cache_instance->sets->lines[dest_j].prefetched;
			}
			else
			{

				vc_instance->q->line.dirty=cache_instance->sets[idx].lines[dest_j].dirty;
				vc_instance->q->line.valid=cache_instance->sets[idx].lines[dest_j].valid;
				vc_instance->q->line.prefetched=cache_instance->sets[idx].lines[dest_j].prefetched;
			}

			vc_instance->q->idx=idx;
			vc_instance->q->next=NULL;

		}
		else {
			while(ptr->next!=NULL){
				ptr=ptr->next;
				++i;
			}
			/*malloc the new node and add to end of vc queue*/
			vc_queue *newnode = malloc(sizeof(vc_queue));
			newnode->line.tag=tag;
			newnode->idx=idx;
			newnode->next=NULL;
			if(cache_instance->type=='d')
			{
				newnode->line.dirty=cache_instance->sets->lines[idx].dirty;
				newnode->line.valid=cache_instance->sets->lines[idx].valid;
				newnode->line.prefetched=cache_instance->sets->lines[idx].prefetched;
			}
			else if(cache_instance->type=='f')
			{
				newnode->line.dirty=cache_instance->sets->lines[dest_j].dirty;
				newnode->line.valid=cache_instance->sets->lines[dest_j].valid;	
				newnode->line.prefetched=cache_instance->sets->lines[dest_j].prefetched;
			}
			else
			{

				newnode->line.dirty=cache_instance->sets[idx].lines[dest_j].dirty;
				newnode->line.valid=cache_instance->sets[idx].lines[dest_j].valid;
				newnode->line.prefetched=cache_instance->sets[idx].lines[dest_j].prefetched;

			}
			ptr->next=newnode;
			/*Free the oldest node in vc if necessary*/
			if(i==((vc_instance->v)-1))
			{
				/*VC is full*/
				vc_queue *old_head = vc_instance->q;
				vc_instance->q=vc_instance->q->next;
				if(old_head->line.dirty==1)
					p_stats->write_backs+=1;
				
				free(old_head);
			}

		}
	}
	else
		ret=0;

	return ret;

}



void cache_replacement(uint64_t tag, uint64_t idx,struct cache_stats_t* p_stats, char access_type)
{
	int i,j,ret;
	insert_lru(tag,idx);
	if(cache_instance->type== 'f') {
				/*The cache is full*/
				lru_block *old_head;
				old_head = cache_instance->sets->lru;
				
				cache_instance->sets->lru = old_head->next;
				for(j=0;cache_instance->sets->lines[j].tag!=old_head->lru_tag;++j);


				if(j<pow(2,cache_instance->s)) {
					/*Block is in cache*/
					if(check_vc(tag,idx)==1)
					{
						/*Swap with vc block if present*/
						swap_blocks_vc(tag,idx,j);
					}
					else {
						
						ret=update_vc(cache_instance->sets->lines[j].tag,idx,j,p_stats);
						if(cache_instance->sets->lines[j].dirty==1 && ret==0)
							p_stats->write_backs+=1;
						p_stats->evictions+=1;
						cache_instance->sets->lines[j].tag=tag;
						cache_instance->sets->lines[j].valid=1;
						cache_instance->sets->lines[j].prefetched=0;					
						if(access_type == 'r') {
							cache_instance->sets->lines[j].dirty=0;
							p_stats->read_misses_combined+=1;
						}
						else {
							p_stats->write_misses_combined+=1;
							cache_instance->sets->lines[j].dirty=1;
						}
					}
				}
				else
				{
					printf("Not inserted into cache but only in lru\n");
				}
				/*Will be treated as a miss*/
				free(old_head);

	}
	else
	{	
				lru_block *old_head;
				old_head = cache_instance->sets[idx].lru;
				cache_instance->sets[idx].lru = old_head->next;
				
				for(j=0;cache_instance->sets[idx].lines[j].tag!=old_head->lru_tag && j<pow(2,cache_instance->s);++j);

					if(j<pow(2,cache_instance->s) ) {
						
						/*to be replaced block is in cache*/
						if(check_vc(tag,idx)==1) {
						
							swap_blocks_vc(tag,idx,j);
							if(cache_instance->sets[idx].lines[j].prefetched==1)
								p_stats->useful_prefetches+=1;
						}
						else {

						
							ret=update_vc(cache_instance->sets[idx].lines[j].tag, idx, j,p_stats);
							if(cache_instance->sets[idx].lines[j].dirty==1 && ret==0)
								p_stats->write_backs+=1;
							p_stats->evictions+=1;
							cache_instance->sets[idx].lines[j].tag=tag;
							cache_instance->sets[idx].lines[j].valid=1;
							cache_instance->sets[idx].lines[j].prefetched=0;
							if(access_type == 'r') {
								p_stats->read_misses_combined+=1;
								cache_instance->sets[idx].lines[j].dirty=0;
							}
							else if(access_type == 'w')
							{
								cache_instance->sets[idx].lines[j].dirty=1;
								p_stats->write_misses_combined+=1;
							
							}
						}				
						

					}
					else
					{
						printf("Not inserted into cache but only in lru\n");
					}
				free(old_head);
		
	
	}

}

void update_lru(uint64_t tag, uint64_t idx) 
{
	int i;
	if(cache_instance->type == 'f') {
		lru_block *node = cache_instance->sets->lru;
		lru_block *prev = NULL;
		while(node!=NULL && node->lru_tag!=tag) {
			prev = node;
			node = node->next;
		}
		if(node->lru_tag==tag)
		{
			if(node->next!=NULL)
			{
				if(prev==NULL)
				{	
					/*first node needs to be moved*/
					cache_instance->sets->lru = node->next;
					node->next=NULL;

				}
				else {
					prev->next = node->next;
					node->next = NULL;
				}
			lru_block *ptr = cache_instance->sets->lru;
			while(ptr->next!=NULL)
				ptr = ptr->next;
			ptr->next = node;
			}
		}


	}

	else {
		lru_block *node = cache_instance->sets[idx].lru;
		lru_block *prev = NULL;
		
		
		while(node!=NULL && node->lru_tag!=tag) {
			prev = node;
			node = node->next;
			
		}
		
		if(node->lru_tag==tag)
		{
			if(node->next!=NULL)
			{
				if(prev==NULL)
				{	
					/*first node needs to be moved*/
					cache_instance->sets[idx].lru = node->next;
					node->next=NULL;

				}
				else {
		
					prev->next = node->next;
					node->next = NULL;
		
				}	
				lru_block *ptr = cache_instance->sets[idx].lru;
				while(ptr->next!=NULL)
					ptr = ptr->next;
		
				ptr->next = node;	
		
				
			}

		}
		

	}
}

int in_cache(uint64_t tag, uint64_t idx, uint64_t offset)
{
	int i,ret=0;
	if(cache_instance->type == 'd') {
		if(cache_instance->sets->lines[idx].tag==tag && cache_instance->sets->lines[idx].valid == 1)
		{	
			ret=1;
    	}
    }
    else if(cache_instance->type == 'f') {
    	for(i=0; cache_instance->sets->lines[i].tag!=tag && i<pow(2,cache_instance->s) ;++i);
    	if(i<pow(2,cache_instance->s) && cache_instance->sets->lines[i].tag==tag && cache_instance->sets->lines[i].valid == 1 ) {
    		ret = 1;
    	}
    }
    else {
    	
    	for(i=0;cache_instance->sets[idx].lines[i].tag!=tag && i<pow(2,cache_instance->s); ++i);
    	
    	if(i<pow(2,cache_instance->s) && cache_instance->sets[idx].lines[i].tag==tag && cache_instance->sets[idx].lines[i].valid == 1) {
    		ret=1;
    	}	

    }
	return ret;
		
	

}


int cache_lookup(uint64_t tag, uint64_t idx, uint64_t offset,struct cache_stats_t* p_stats, char access_type )
{
	int i,ret=0;
	if(cache_instance->type == 'd') {
		if(cache_instance->sets->lines[idx].tag==tag && cache_instance->sets->lines[idx].valid == 1)
		{	
			if(access_type=='w')
				cache_instance->sets->lines[idx].dirty=1;
			if(cache_instance->sets->lines[idx].prefetched==1) {
				p_stats->useful_prefetches+=1;
				cache_instance->sets->lines[idx].prefetched=0;
			}
    		ret=1;
    	}
    }
    else if(cache_instance->type == 'f') {
    	for(i=0; cache_instance->sets->lines[i].tag!=tag && i<pow(2,cache_instance->s) ;++i);
    	if(i<pow(2,cache_instance->s) && cache_instance->sets->lines[i].tag==tag && cache_instance->sets->lines[i].valid == 1 ) {
    		if(access_type == 'w')
    			cache_instance->sets->lines[i].dirty=1;
    		if(cache_instance->sets->lines[i].prefetched==1) {
				p_stats->useful_prefetches+=1;
				cache_instance->sets->lines[i].prefetched=0;
			}
    		update_lru(cache_instance->sets->lines[i].tag, 0);
    		ret = 1;
    	}
    }
    else {
    	
    	for(i=0;cache_instance->sets[idx].lines[i].tag!=tag && i<pow(2,cache_instance->s); ++i);
    	
    	if(i<pow(2,cache_instance->s) && cache_instance->sets[idx].lines[i].tag==tag && cache_instance->sets[idx].lines[i].valid == 1) {
    	
    		if(access_type == 'w')
    			cache_instance->sets[idx].lines[i].dirty=1;
    		if(cache_instance->sets[idx].lines[i].prefetched==1) {
				p_stats->useful_prefetches+=1;
				
			}
			cache_instance->sets[idx].lines[i].prefetched=0;
    		update_lru(cache_instance->sets[idx].lines[i].tag, idx);
    		ret=1;
    	}	

    }
	return ret;
	
}


void cache_replacement_lru(uint64_t tag, uint64_t idx, struct cache_stats_t* p_stats, char access_type)
{
	int i,j,ret;
	if(cache_instance->type=='d')
	{
		if(check_vc(tag,idx)==1)
		{
			swap_blocks_vc(tag,idx,0);
		}
		else
		{
			update_vc(cache_instance->sets->lines[idx].tag,idx,0,p_stats);
			cache_instance->sets->lines[idx].tag = tag;
			cache_instance->sets->lines[idx].valid = 1;
			cache_instance->sets->lines[idx].prefetched=1;
				cache_instance->sets->lines[idx].dirty = 0;
			
			
		}

	}

	else if(cache_instance->type== 'f') {
			lru_block *old_head;
			lru_block *new_head;
			old_head = cache_instance->sets->lru;
				new_head=malloc(sizeof(lru_block*));
				new_head->lru_tag = tag;
				if(old_head==NULL)
					new_head->next = NULL;
				else
					new_head->next=old_head->next;
				cache_instance->sets->lru = new_head;
				for(j=0;cache_instance->sets->lines[j].tag!=old_head->lru_tag;++j);


				if(j<pow(2,cache_instance->s)) {
					/*Block is in cache*/
					if(check_vc(tag,idx)==1)
					{
						/*Swap with vc block if present*/
						swap_blocks_vc(tag,idx,j);
					}
					else {
						
						ret=update_vc(cache_instance->sets->lines[j].tag,idx,j,p_stats);
						if(cache_instance->sets->lines[j].dirty==1 && ret==0)
							p_stats->write_backs+=1;
						p_stats->evictions+=1;
						cache_instance->sets->lines[j].tag=tag;
						cache_instance->sets->lines[j].valid=1;
						cache_instance->sets->lines[j].prefetched=1;			
							cache_instance->sets->lines[j].dirty=0;
						}
				}
				else
				{
					printf("%d Not inserted into cache but only in lru\n",__LINE__);
				}
				/*Will be treated as a miss*/
				free(old_head);
	

	}


	else {
				lru_block* old_head;
				lru_block *new_head;
				old_head = cache_instance->sets[idx].lru;

				new_head=malloc(sizeof(lru_block*));

				new_head->lru_tag = tag;


				if(old_head==NULL) {
					/*Cache isn't full yet*/
					new_head->next = NULL;
					for(j=0;cache_instance->sets[idx].lines[j].valid!=0 && j<pow(2,cache_instance->s); ++j);
						if(j<pow(2,cache_instance->s)) {
								cache_instance->sets[idx].lines[j].tag=tag;
								cache_instance->sets[idx].lines[j].valid=1;
								cache_instance->sets[idx].lines[j].prefetched=1;
									cache_instance->sets[idx].lines[j].dirty=0;
				   		}
				   		
				}
				else {
				new_head->next=old_head->next;
				for(j=0;cache_instance->sets[idx].lines[j].tag!=old_head->lru_tag && j<pow(2,cache_instance->s);++j);

					if(j<pow(2,cache_instance->s) ) {

						/*to be replaced block is in cache*/
						if(check_vc(tag,idx)==1) {

							swap_blocks_vc(tag,idx,j);
						}
						else {


							ret=update_vc(cache_instance->sets[idx].lines[j].tag, idx, j,p_stats);
							if(cache_instance->sets[idx].lines[j].dirty==1 && ret==0)
								p_stats->write_backs+=1;
							p_stats->evictions+=1;
							cache_instance->sets[idx].lines[j].tag=tag;
							cache_instance->sets[idx].lines[j].valid=1;
							cache_instance->sets[idx].lines[j].prefetched=1;
								cache_instance->sets[idx].lines[j].dirty=0;


						}				
						
						free(old_head);
					}

					
					else
					{
						printf("%d Not inserted into cache but only in lru\n",__LINE__);
					}

				}
				cache_instance->sets[idx].lru = new_head;
				
	}
	

}




int prefetcher_func(uint64_t block, struct cache_stats_t* p_stats, char access_type)
{
	int ret=0,i=1;
	if(pf_instance->k > 0) {
		int d;
		uint64_t curr_prefetch_addr;
		uint64_t tag,idx;

		int diff = 64 - MEMADDR;
		int tag_bits = MEMADDR - ( cache_instance->idx_bits);

		d = block - pf_instance->last_miss_baddr;
		if(d == pf_instance->pending_stride)
		{
			
			for(i=1;i<=pf_instance->k;++i)
			{
				p_stats->prefetched_blocks+=1;
				curr_prefetch_addr=block+i*d;
				idx =  (curr_prefetch_addr << (tag_bits ));

				idx = idx >> (tag_bits);

				tag = curr_prefetch_addr >> (cache_instance->idx_bits);
				if(in_cache(tag,idx,0)!=1)				
				{

					cache_replacement_lru(tag,idx,p_stats,access_type);
					ret=1;
				}

			}

		}
		pf_instance->last_miss_baddr = block ;
		pf_instance->pending_stride=d;
	}
	return ret;
}


void write_dirty(uint64_t tag, uint64_t idx, uint64_t offset)
{
	int i=0;
	if(cache_instance->type=='d')
		cache_instance->sets->lines[idx].dirty=1;
	
	else if(cache_instance->type == 'f')
	{
		for(i=0;cache_instance->sets->lines[i].tag!=tag;++i);
		if(i<pow(2,cache_instance->s)){
			cache_instance->sets->lines[i].dirty=1;
		}
		else
			printf("Fetch isn't working right\n");
	}
	else
	{
		for(i=0;cache_instance->sets[idx].lines[i].tag!=tag;++i);
		if(i<pow(2,cache_instance->s)){
			cache_instance->sets[idx].lines[i].dirty=1;
		}
		else
			printf("Fetch isn't working right\n");

	}

}

void fetch_addr(uint64_t tag, uint64_t idx, uint64_t offset, struct cache_stats_t* p_stats, char access_type)
{
	int i,  replace = 1;
	if(cache_instance->type == 'd') {
		if(check_vc(tag,idx)==1)
		{
			swap_blocks_vc(tag,idx,0);
					
		}
		else
		{
			update_vc(cache_instance->sets->lines[idx].tag,idx,0,p_stats);
			cache_instance->sets->lines[idx].tag = tag;
			cache_instance->sets->lines[idx].valid = 1;
			cache_instance->sets->lines[idx].prefetched=0;
					
			if(access_type == 'r') {
				p_stats->read_misses_combined+=1;
				cache_instance->sets->lines[idx].dirty = 0;
			}
			else {
				cache_instance->sets->lines[idx].dirty = 1;	
				p_stats->write_misses_combined+=1;
			}
		}
		
		
	}
	else if (cache_instance->type == 'f') {
		for(i=0; cache_instance->sets->lines[i].valid != 0 && i<pow(2,cache_instance->s); ++i); 
			if(cache_instance->sets->lines[i].valid == 0) {
				if(check_vc(tag,idx)==1)
				{
					swap_blocks_vc(tag,idx,i);
					
				}
				else {
					
					cache_instance->sets->lines[i].tag = tag;
					cache_instance->sets->lines[i].valid = 1;
					cache_instance->sets->lines[i].prefetched=0;
					
					if(access_type == 'r') {
						cache_instance->sets->lines[i].dirty = 0;
						p_stats->read_misses_combined+=1;
					}
					else {
						cache_instance->sets->lines[i].dirty = 1;
						p_stats->write_misses_combined+=1;
					}
				}
				insert_lru(tag,0);
			}
			else
			{
				
				cache_replacement(tag,idx,p_stats,access_type);
				
			}
			
	}
	else {
		for(i=0; cache_instance->sets[idx].lines[i].valid != 0 && i<pow(2,cache_instance->s);++i); 
			if(cache_instance->sets[idx].lines[i].valid == 0) {

				if(check_vc(tag,idx)==1)
				{

					
					swap_blocks_vc(tag,idx,i);
					if(cache_instance->sets[idx].lines[i].prefetched==1)
						p_stats->useful_prefetches+=1;
				}
				else {

					
					
					cache_instance->sets[idx].lines[i].tag=tag;
					cache_instance->sets[idx].lines[i].valid=1;
					cache_instance->sets[idx].lines[i].prefetched=0;
					if(access_type == 'r') {
						cache_instance->sets[idx].lines[i].dirty = 0;
						p_stats->read_misses_combined+=1;
					}
					else if(access_type='w') {
						cache_instance->sets[idx].lines[i].dirty = 1;
						p_stats->write_misses_combined+=1;
					}
				}

				insert_lru(tag,idx);
			}
			else 
			{

				cache_replacement(tag,idx,p_stats,access_type);
			}
			
			
	}
	

}

/**
 * Subroutine that simulates the cache one trace event at a time.
 * XXX: You're responsible for completing this routine
 *
 * @rw The type of event. Either READ or WRITE
 * @address  The target memory address
 * @p_stats Pointer to the statistics structure
 */
void cache_access(char rw, uint64_t address, struct cache_stats_t* p_stats) {
	int diff = 64 - MEMADDR;
	int tag_bits = MEMADDR - (cache_instance->b + cache_instance->idx_bits);
	uint64_t offset = (address << (diff + tag_bits + cache_instance->idx_bits));
	offset =  offset >> (diff + tag_bits + cache_instance->idx_bits);
	uint64_t idx =  (address << (tag_bits + diff));
	idx = idx >> (diff + tag_bits + cache_instance->b);
	uint64_t tag = address >> (cache_instance->idx_bits + cache_instance->b );
	uint64_t block = address >> (cache_instance->b);
	
	switch(rw) {
		case 'r': 	p_stats->accesses+=1;
					p_stats->reads+=1;
					if(cache_lookup(tag, idx, offset, p_stats, rw)!=1) { /*Miss*/
						p_stats->read_misses+=1;
						fetch_addr(tag, idx, offset, p_stats, rw);	
						prefetcher_func(block,p_stats,rw);
					}

					break;
		case 'w':	p_stats->accesses+=1;
					p_stats->writes+=1;
					if(cache_lookup(tag,idx,offset, p_stats, rw)!=1) {
						p_stats->write_misses+=1;
						fetch_addr(tag,idx,offset, p_stats, rw);
						prefetcher_func(block,p_stats,rw);
					}
					break;
		default: fprintf(stderr,"ERROR: Operation not supported\n");
				 break;
	}	

}

/**
 * Subroutine for cleaning up any outstanding memory operations and calculating overall statistics
 * such as miss rate or average access time.
 * XXX: You're responsible for completing this routine
 *
 * @p_stats Pointer to the statistics structure
 */
void complete_cache(struct cache_stats_t *p_stats) {
	
	p_stats->misses = p_stats->read_misses + p_stats->write_misses;
	p_stats->vc_misses = p_stats->read_misses_combined + p_stats->write_misses_combined;
	p_stats->hit_time = 2 + 0.2*cache_instance->s;
	p_stats->miss_penalty = 200;
	p_stats->miss_rate = (double) p_stats->misses / p_stats->accesses;
	p_stats->avg_access_time = p_stats->hit_time + (p_stats->miss_rate*p_stats->miss_penalty);
	p_stats->bytes_transferred = (p_stats->write_backs + p_stats->vc_misses +  p_stats->prefetched_blocks ) *  pow(2,cache_instance->b);
	free(cache_instance);
	free(vc_instance);
}
