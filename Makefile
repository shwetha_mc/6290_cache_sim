CXXFLAGS := -g -Wall -std=c++0x -lm -g
CXX=gcc
LIBS = -lrt -lm

all: cachesim

cachesim: cachesim.o cachesim_driver.o
	$(CXX) $(CXXFLAGS) -o cachesim cachesim.o cachesim_driver.o $(LIBS)

clean:
	rm -f cachesim *.o
